import io

import slack
import os
from pathlib import Path
from dotenv import load_dotenv
from flask import Flask, request, Response
from slackeventsapi import SlackEventAdapter
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from googleapiclient.http import MediaFileUpload
from googleapiclient.http import MediaIoBaseDownload
import json
import itertools
from GoogleServer import GoogleDriveLoader
import plotly.express as px
import plotly
import plotly.graph_objects as go
from plotly.subplots import make_subplots
from plotly.offline import plot
from PIL import Image

import threading

env_path = Path('.') / '.env'
load_dotenv(dotenv_path=env_path)

app = Flask(__name__)
slack_event_adapter = SlackEventAdapter(os.getenv("SIGNING_SECRET"), '/slack/events', app)
client = slack.WebClient(token=os.getenv("SLACK_TOKEN"))
# BOT_ID = client.api_call("auth.test")['user_id']
loader = GoogleDriveLoader(client, True)


@app.route('/calculate_budget', methods=['POST'])
def message_count():
    data = request.form
    param = data.get('text')
    budget = None
    if param:
        budget = CalculateBudget(param)
    channel_id = data.get('channel_id')
    client.chat_postMessage(channel=channel_id, text=budget)
    return Response(), 200


@app.route('/bot_comparecampaign', methods=['POST'])
def bot_compare():
    data = request.form
    param = data.get('text')
    channel_id = data.get('channel_id')

    def drawing():
        company_list = param.split()
        company_results = []
        campaign_label = GetCampaingLabels(company_list[0]);
        for company in company_list:
            ret_val = GetCampaingResults(company)
            company_results.append(ret_val)

        DrawCharts(company_results, campaign_label,channel_id)

    if param:
        # Drawing operation may take long time, so pass the operation other thread and response 200 immediately
        thread = threading.Thread(target=drawing)
        thread.start()
        # Response
        client.chat_postMessage(channel=channel_id, text="Starting the drawing ...")
        return Response(), 200
    else:
        client.chat_postMessage(channel=channel_id, text="No campaign name is entered")
        return Response(), 200


def CalculateBudget(company_name):
    credential = loader.Authentication()
    service = build('drive', 'v3', credentials=credential)
    _company_name = "Campaign_" + str(company_name)
    results = service.files().list(pageSize=20, fields="nextPageToken, files(id, name)").execute()
    items = results.get('files', [])

    if not items:
        print('No files found.')
        return
    for item in items:
        if item['name'] == _company_name:
            spread_sheet_id = item['id']
            service = build('sheets', 'v4', credentials=credential)
            sheet = service.spreadsheets()
            e2_result = sheet.values().get(spreadsheetId=spread_sheet_id,
                                           range="G2:G").execute()
            e_data = json.dumps(e2_result)
            budget_value = json.loads(e_data)['values']
            budget_value = list(itertools.chain(*budget_value))
            return _company_name + str(" Budget Value: ") + str(budget_value[0])

    return _company_name + str(" not found !!!")


def GetCampaingResults(company_name):
    credential = loader.Authentication()
    service = build('drive', 'v3', credentials=credential)
    _company_name = "Campaign_" + str(company_name)
    results = service.files().list(pageSize=20, fields="nextPageToken, files(id, name)").execute()
    items = results.get('files', [])

    if not items:
        print('No files found.')
        return
    campaing_results = []
    for item in items:
        if item['name'] == _company_name:
            spread_sheet_id = item['id']
            service = build('sheets', 'v4', credentials=credential)
            sheet = service.spreadsheets()
            e2_result = sheet.values().get(spreadsheetId=spread_sheet_id,
                                           range="A2:G2").execute()
            e_data = json.dumps(e2_result)
            campaing_results = json.loads(e_data)['values']
            campaing_results = list(itertools.chain(*campaing_results))
            break

    return campaing_results


def GetCampaingLabels(company_name):
    credential = loader.Authentication()
    service = build('drive', 'v3', credentials=credential)
    _company_name = "Campaign_" + str(company_name)
    results = service.files().list(pageSize=20, fields="nextPageToken, files(id, name)").execute()
    items = results.get('files', [])

    if not items:
        print('No files found.')
        return
    campaing_label = []
    for item in items:
        if item['name'] == _company_name:
            spread_sheet_id = item['id']
            service = build('sheets', 'v4', credentials=credential)
            sheet = service.spreadsheets()
            e2_result = sheet.values().get(spreadsheetId=spread_sheet_id,
                                           range="A1:G1").execute()
            e_data = json.dumps(e2_result)
            campaing_label = json.loads(e_data)['values']
            campaing_label = list(itertools.chain(*campaing_label))
            break

    return campaing_label


def DrawCharts(campaign_results, campaign_labels,channel_id):
    number_of_charts = len(campaign_results)
    campaign_labels.pop(0)
    if number_of_charts == 0:
        return
    _specs = []
    titles = []
    _specs.append([])
    for i in range(number_of_charts):
        _specs[0].append({'type': 'domain'})
        titles.append(campaign_results[i][0])
    fig = make_subplots(rows=1, cols=number_of_charts, specs=_specs, subplot_titles=titles)

    for i in range(number_of_charts):
        campaign_name = campaign_results[i].pop(0)
        values = campaign_results[i]
        labels = campaign_labels
        fig.add_trace(go.Pie(labels=labels, values=values, name=campaign_name), 1, i + 1)

    fig.update_traces(hole=.4, hoverinfo="label+percent+name")
    img_bytes = fig.to_image(format="png", engine="orca")
    fig.show()
    client.files_upload(channels=channel_id, title='chart', initial_comment='Campaigns Chart', file=io.BytesIO(img_bytes))


if __name__ == "__main__":
    app.run(debug=True, use_reloader=False)
