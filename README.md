# Slack boot app
The bot is developed for monitoring data uploading to google drive and also two bot command 
as /calculate_budget and /bot_comparecampaign are available.

### /calculate_budget
Gets only one parameter as a campaign name and return budget value

### /bot_comparecampaign
Gets different campaing names as parameters and draw charts of them and also return png version of the charts as a reply to slack channel

##### To use app
Get SLACK_TOKEN and SIGNING_TOKEN from slack api and paste them to .env file
Get client_secret.json file from google cloud to use google drive api and google spreadseet api and paste it to directory. 
When application is run once, token.json file is created automatically  



Python Flask is used as a backend framework.


