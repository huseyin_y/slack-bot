from __future__ import print_function

import json
import os.path
from email.errors import MessageDefect

from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from googleapiclient.http import MediaFileUpload
from googleapiclient.http import MediaIoBaseDownload
# import pandas as pd
import json
import itertools
import io

from io import BytesIO
# If modifying these scopes, delete the file token.json.
SCOPES = ['https://www.googleapis.com/auth/drive']
NUMBER_OF_CAMPAIGN = 0

channel_id  = '#alictus-demo'

class GoogleDriveLoader:
    def __init__(self, client, load):
        if load:
            client.chat_postMessage(channel=channel_id, text="Campaign bot is initializing,"
                                                             " just wait a second ...")

            client.chat_postMessage(channel=channel_id, text="Authentication is establishing ...")
            credential = self.Authentication()
            client.chat_postMessage(channel=channel_id, text="Authentication is established")

            client.chat_postMessage(channel=channel_id, text="Folder is creating with name Data ...")
            # 1. Create a folder that contains the data in Google Drive via API.
            folder_id = self.CreateFolder(credential)
            client.chat_postMessage(channel=channel_id, text="Folder is created with name Data")

            client.chat_postMessage(channel=channel_id, text="The dataset is uploading to Google Drive ...")
            # 2. Upload the dataset to Google Drive via Google Drive API in spreadsheet format.
            file_id = self.UploadData(credential, folder_id)
            client.chat_postMessage(channel=channel_id, text="The dataset is uploaded to Google Drive")

            client.chat_postMessage(channel=channel_id, text="Total budget is calculating for each campaign"
                                                          " and separate spreadsheets are creating ... ")
            # 3. Create separate spreadsheet files for each campaign row in the dataset via Google Sheets API.
            self.CalculateTotalBudget(credential, file_id)
            self.CreateSeperateFiles(credential, folder_id, file_id)
            client.chat_postMessage(channel=channel_id, text="Total budget is calculated for each campaign"
                                                          " and separate spreadsheets are created")

            client.chat_postMessage(channel=channel_id, text="Campaign bot is here now !!!")
        print('Initialization is completed')

    def Authentication(self):
        creds = None
        # The file token.json stores the user's access and refresh tokens, and is
        # created automatically when the authorization flow completes for the first
        # time.
        if os.path.exists('token.json'):
            creds = Credentials.from_authorized_user_file('token.json', SCOPES)
        # If there are no (valid) credentials available, let the user log in.
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    'client_secret.json', SCOPES)
                creds = flow.run_local_server(port=0)
            # Save the credentials for the next run
            with open('token.json', 'w') as token:
                token.write(creds.to_json())

        return creds

    def CreateFolder(self,credential):
        service = build('drive', 'v3', credentials=credential)

        file_metadata = {
            'name': 'Data',
            'mimeType': 'application/vnd.google-apps.folder'
        }
        file = service.files().create(body=file_metadata,
                                      fields='id').execute()
        folder_id = file.get('id');
        # print('Folder ID: %s' % folder_id)
        return folder_id

    def UploadData(self,credential, folder_id):
        service = build('drive', 'v3', credentials=credential)

        file_metadata = {
            'name': 'AlictusDataSet.xlsx',
            'mimeType': 'application/vnd.google-apps.spreadsheet',
            'parents': [folder_id]
        }
        media_content = MediaFileUpload('./data/AlictusDataSet.xlsx',
                                        mimetype='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')

        file = service.files().create(
            body=file_metadata,
            media_body=media_content,
            fields='id'
        ).execute()
        file_id = file.get('id')
        # print('File ID: %s' % file_id)
        return file_id

    def CalculateTotalBudget(self,credential,file_id):
        global NUMBER_OF_CAMPAIGN

        service = build('sheets', 'v4', credentials=credential)
        sheet = service.spreadsheets()
        c2_result = sheet.values().get(spreadsheetId=file_id,
                                       range="C2:C").execute()
        c_data = json.dumps(c2_result)
        c_data_dictionary = json.loads(c_data)
        c_data_list = c_data_dictionary['values']
        c_data_list = list(itertools.chain(*c_data_list))
        c_data_list_num = [int(i) for i in c_data_list]
        NUMBER_OF_CAMPAIGN = len(c_data_list_num)

        e2_result = sheet.values().get(spreadsheetId=file_id,
                                       range="E2:E").execute()
        e_data = json.dumps(e2_result)
        e_data_dictionary = json.loads(e_data)
        e_data_list = e_data_dictionary['values']
        e_data_list = list(itertools.chain(*e_data_list))
        e_data_list_num = [float(i) for i in e_data_list]

        total_budget = [a * b for a, b in zip(c_data_list_num, e_data_list_num)]
        spreadsheet = service.spreadsheets().get(spreadsheetId=file_id).execute()
        sheet_id = None
        for _sheet in spreadsheet['sheets']:
            sheet_id = _sheet['properties']['sheetId']
            break

        total_budget_str = ['{:.2f}'.format(x) for x in total_budget]
        total_budget_str.insert(0, 'Total Budget')
        _total_budget=  [[el] for el in total_budget_str]

        # Append the data
        resource = {
            "majorDimension": "ROWS",
            "values": _total_budget
        }
        service.spreadsheets().values().append(spreadsheetId=file_id, range="Sheet1!G:G",
                                               body=resource, valueInputOption="USER_ENTERED").execute()
    def CreateSeperateFiles(self,credential,folder_id,file_id):
        # get campaign name firstly,
        service = build('sheets', 'v4', credentials=credential)
        sheet = service.spreadsheets()
        _range = "A1:G"+str(NUMBER_OF_CAMPAIGN + 1)
        c2_result = sheet.values().get(spreadsheetId=file_id,
                                       range=_range).execute()
        c_data = json.dumps(c2_result)
        c_data_dictionary = json.loads(c_data)
        c_data_list = c_data_dictionary['values']
        _c_data_list_first_row = c_data_list[0]

        c_data_list.pop(0)
        # print(c_data_list)

        service = build('drive', 'v3', credentials=credential)
        spreadsheet_id_list = []

        for fn in c_data_list:
            file_name = 'Campaign_' + fn[0]
            file_metadata = {
                'name': file_name,
                'mimeType': 'application/vnd.google-apps.spreadsheet',
                'parents': [folder_id]
            }
            file = service.files().create(body=file_metadata, fields='id').execute()
            spreadsheet_id_list.append(file.get('id'))

        # Populate spreadsheets with identifier and data row
        service = build('sheets', 'v4', credentials=credential)
        sheet = service.spreadsheets()

        c_data_list_first_row = []
        c_data_list_first_row.append(_c_data_list_first_row)

        temp_row_data = []
        for i in range(NUMBER_OF_CAMPAIGN):
            resource = {
                "values": c_data_list_first_row
            }
            sheet.values().append(spreadsheetId=spreadsheet_id_list[i], range="A1:G1", insertDataOption="INSERT_ROWS",
                                  body=resource, valueInputOption="USER_ENTERED").execute()
            temp_row_data.append(c_data_list[i])
            sheet.values().append(spreadsheetId=spreadsheet_id_list[i], range="A2:G2", insertDataOption="INSERT_ROWS",
                                  body={"values": temp_row_data}, valueInputOption="USER_ENTERED").execute()
            temp_row_data.clear()
